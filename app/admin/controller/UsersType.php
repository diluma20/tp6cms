<?php

namespace app\admin\controller;

// 引入框架内置类
use think\facade\Request;

// 引入表格和表单构建器
use app\common\facade\MakeBuilder;
use app\common\builder\FormBuilder;
use app\common\builder\TableBuilder;

/**
 * 会员分组控制器
 */
class UsersType extends Base
{
    // 验证器
    protected string $validate = 'UsersType';

    // 当前主表
    protected string $tableName = 'users_type';

    // 当前主模型
    protected string $modelName = 'UsersType';

}
