<?php

namespace app\admin\controller;

// 引入框架内置类
use think\facade\Request;

// 引入表格和表单构建器
use app\common\facade\MakeBuilder;
use app\common\builder\FormBuilder;
use app\common\builder\TableBuilder;

/**
 * 留言模块控制器
 */
class Message extends Base
{
    // 验证器
    protected string $validate = 'Message';

    // 当前主表
    protected string $tableName = 'message';

    // 当前主模型
    protected string $modelName = 'Message';

}
