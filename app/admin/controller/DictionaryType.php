<?php

namespace app\admin\controller;

// 引入框架内置类
use think\facade\Request;

// 引入表格和表单构建器
use app\common\facade\MakeBuilder;
use app\common\builder\FormBuilder;
use app\common\builder\TableBuilder;

/**
 * 字典类型控制器
 */
class DictionaryType extends Base
{
    // 验证器
    protected string $validate = 'DictionaryType';

    // 当前主表
    protected string $tableName = 'dictionary_type';

    // 当前主模型
    protected string $modelName = 'DictionaryType';

}
