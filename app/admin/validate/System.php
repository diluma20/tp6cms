<?php

namespace app\admin\validate;

use think\Validate;

/**
 * 系统设置验证器
 */
class System extends Validate
{
    protected $rule = [
        'copyright|版权信息' => [
            'max' => '255',
        ],
        'upload_driver|上传驱动' => [
            'require' => 'require',
        ],
        'upload_file_size|文件限制' => [
            'max' => '50',
        ],
        'upload_image_size|图片限制' => [
            'max' => '50',
        ]
    ];
}