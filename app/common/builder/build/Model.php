<?php

namespace app\common\model;

/**
 * {$comment}模型
 */
class {$modelName} extends Base
{
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    {$modulePk}
    {$moduleTable}
    {$relations}

}