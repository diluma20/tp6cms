<?php

namespace app\admin\validate;

use think\Validate;

/**
 * {$comment}验证器
 */
class {$modelName} extends Validate
{
    {$rules}
}