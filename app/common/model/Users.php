<?php

namespace app\common\model;

/**
 * 会员管理模型
 */
class Users extends Base
{
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    
    
    public function usersType()
    {
        return $this->belongsTo('UsersType', 'type_id');
    }
    

}