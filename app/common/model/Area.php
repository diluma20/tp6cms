<?php

namespace app\common\model;

/**
 * 省市区联动模型
 */
class Area extends Base
{
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
}