<?php

namespace app\common\model;

/**
 * 团队模块模型
 */
class Team extends Base
{
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    
    
    public function cate()
    {
        return $this->belongsTo('Cate', 'cate_id');
    }
    public function usersType()
    {
        return $this->belongsTo('UsersType', 'view_auth');
    }
    

}