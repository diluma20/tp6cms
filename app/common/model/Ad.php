<?php

namespace app\common\model;

/**
 * 广告管理模型
 */
class Ad extends Base
{
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    
    
    public function adType()
    {
        return $this->belongsTo('AdType', 'type_id');
    }
    

}