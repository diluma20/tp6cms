<?php

namespace app\common\model;

/**
 * 字段分组模型
 */
class FieldGroup extends Base
{
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    
    
    public function module()
    {
        return $this->belongsTo('Module', 'module_id');
    }
    

}