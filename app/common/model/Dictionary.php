<?php

namespace app\common\model;

/**
 * 字典数据模型
 */
class Dictionary extends Base
{
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    
    
    public function dictionaryType()
    {
        return $this->belongsTo('DictionaryType', 'dict_type');
    }
    

}