## DLMCMS V6.1 快速开发内容管理系统

[![star](https://gitee.com/diluma20/tp6cms/badge/star.svg?theme=dark)](https://gitee.com/diluma20/tp6cms/stargazers)
[![fork](https://gitee.com/diluma20/tp6cms/badge/fork.svg?theme=gray)](https://gitee.com/diluma20/tp6cms/members)
![PHP Version](https://img.shields.io/badge/php-%3E%3D7.2.5-orange)
[![License](https://img.shields.io/badge/license-MIT-brightgreen)](https://gitee.com/ruoshuiyx/tp6/blob/master/LICENSE)

DLMCMS 基于 ThinkPHP6.1 + AdminLTE 开发，简单 / 易用 / 响应式 / 低门槛。

系统内置了表单构建器和表格构建器，配合后台模块管理和字段管理能快速方便的构建Web应用程序。


## 安装使用

> 下载DLMCMS完整包解压到你本地（建议采用git方式拉取，或宝塔一键部署）

> 将你的站点绑定到public目录(强烈建议绑定到public目录)

> 将文件夹中/public/data/diluma_tp6cms.sql还原到你自己项目的数据库中（需自行创建数据库）

> 修改.env 中数据库配置信息（调试模式也是在这个文件中进行打开和关闭）

> 访问后台并登录查看是否正常，后台目录为http://www.yourwebsite.com/admin （如无法访问请尝试隐藏index.php）

> 默认后台用户名：admin 密码：dlm123456

## 在线体验
- 用户名：demo 密码：123456
- 陆续收到一些捐赠、授权和打赏，谢谢各位小伙伴的支持。

演示地址：
[http://tp6cms.848981.net/admin](http://tp6cms.848981.net/admin)


## 演示图

<table>
    <tr>
        <td><img src="https://diluma.oss-cn-shanghai.aliyuncs.com/tp6cms/diluma-tp6cms-1.png"/></td>
        <td><img src="https://diluma.oss-cn-shanghai.aliyuncs.com/tp6cms/diluma-tp6cms-2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://diluma.oss-cn-shanghai.aliyuncs.com/tp6cms/diluma-tp6cms-3.png"/></td>
        <td><img src="https://diluma.oss-cn-shanghai.aliyuncs.com/tp6cms/diluma-tp6cms-4.png"/></td>
    </tr>
    <tr>
        <td><img src="https://diluma.oss-cn-shanghai.aliyuncs.com/tp6cms/diluma-tp6cms-5.png"/></td>
        <td><img src="https://diluma.oss-cn-shanghai.aliyuncs.com/tp6cms/diluma-tp6cms-6.png"/></td>
    </tr>
	<tr>
        <td><img src="https://diluma.oss-cn-shanghai.aliyuncs.com/tp6cms/diluma-tp6cms-7.png"/></td>
        <td><img src="https://diluma.oss-cn-shanghai.aliyuncs.com/tp6cms/diluma-tp6cms-8.png"/></td>
    </tr>	 
    <tr>
        <td><img src="https://diluma.oss-cn-shanghai.aliyuncs.com/tp6cms/diluma-tp6cms-9.png"/></td>
        <td><img src="https://diluma.oss-cn-shanghai.aliyuncs.com/tp6cms/diluma-tp6cms-10.png"/></td>
    </tr>
	<tr>
        <td><img src="https://diluma.oss-cn-shanghai.aliyuncs.com/tp6cms/diluma-tp6cms-11.png"/></td>
        <td><img src="https://diluma.oss-cn-shanghai.aliyuncs.com/tp6cms/diluma-tp6cms-12.png"/></td>
    </tr>
	<tr>
        <td><img src="https://diluma.oss-cn-shanghai.aliyuncs.com/tp6cms/diluma-tp6cms-13.png"/></td>
        <td><img src="https://diluma.oss-cn-shanghai.aliyuncs.com/tp6cms/diluma-tp6cms-14.png"/></td>
    </tr>
	<tr>
        <td><img src="https://diluma.oss-cn-shanghai.aliyuncs.com/tp6cms/diluma-tp6cms-15.png"/></td>
        <td><img src="https://diluma.oss-cn-shanghai.aliyuncs.com/tp6cms/diluma-tp6cms-16.png"/></td>
    </tr>
</table>


## 版权信息

请尊重DLMCMS开发者的劳动成果，DLMCMS提供免费使用，但未授权前请保留前台 Powered by DLMCMS ，并不得修改后台版权信息。